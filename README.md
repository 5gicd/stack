# EPI-TEAM Nuit de l'info

## GIT
Notre solution se compose de 4 repo GIT:
Backend https://gitlab.com/5gicd/backend
Frontend https://gitlab.com/5gicd/frontend
Simulator https://gitlab.com/5gicd/simulator
Stack https://gitlab.com/5gicd/stack

## Images Docker
En plus des repos GIT, nous avons hébergé notre solution sur DockerHub:
https://hub.docker.com/r/jaceromri/nuit_back/
https://hub.docker.com/r/jaceromri/nuit_front/
https://hub.docker.com/r/jaceromri/nuit_sim/

## Script de déploiement
Enfin, pour faciliter le déploiement, nous avons conçu le fichier de description de déploiement “docker-compose.yml” dans le repo “stack” avec un script bash pour initialiser le serveur.

Les étapes d’initialisation sont:
Installation de docker et docker compose
Initialisation de docker swarm sur le serveur
Initialisation du volume docker pour stocker les données
Et enfin lancer un stack docker
