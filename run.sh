if [ -x "$(command -v docker)" ]; then
    echo "docker already installed"
else
    echo "Install docker"
    curl -fsSL https://get.docker.com | sh
fi

if [ -x "$(command -v docker-compose)" ]; then
    echo "docker-compose already installed"
else
    echo "Install docker-compose"
    curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
fi

echo "running stack"
docker swarm init

docker volume create storage_dir
docker stack deploy nuit --compose-file docker-compose.yml